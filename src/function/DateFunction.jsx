export function timeConverter(UNIX_timestamp){
    var event = new Date(UNIX_timestamp);
    const options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'};
    var time = event.toLocaleDateString(undefined, options);
    return time;
  }

export const parse = require('html-react-parser');