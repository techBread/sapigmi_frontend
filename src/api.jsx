import axios, { AxiosHeaders } from 'axios';

export const BASE_URL = `http://localhost:8080/api/v1`
const api = axios.create({
  baseURL: BASE_URL,
})

api.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;
  return config;

})

api.interceptors.response.use((config) => {
  return config;
}, async (error) => {
  const originalRequest = error.config
  if(error.response.status === 401 && error.config && !error.config._isRetry) {
    originalRequest._isRetry = true;
    try {
      console.log("начал отправку рефреш запроса")
      const response = await axios.get(BASE_URL + '/auth/refresh-token', {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('refresh_token')}`
          }   
      })
      const data = response.data;
      
      console.log("Я отправил рефреш запрос")
      localStorage.setItem('token', data.access_token)
      localStorage.setItem('refresh_token', data.refresh_token)
      return api.request(originalRequest)
    } catch {
      console.log("Не авторизован")
    }
    throw error;
  }
})

export default api;