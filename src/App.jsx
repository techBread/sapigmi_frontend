import {BrowserRouter, Route, Routes, createBrowserRouter , } from 'react-router-dom';
import ErrorPage from './component/common/ErrorPage';
import Root from './component/root/Root';
import ProjectsPage from './component/project/projectPage/ProjectPage';
import { createContext, useEffect, useState } from 'react';
import ProtectedRoute from './component/root/ProtectedRoot';
import { ToastContainer } from 'react-toastify';
import api, { BASE_URL } from './api';
import axios from 'axios';
import SurveyPlacePage from './component/surveyPlace/surveyPlacePage';
import AuthModal from './component/auth/AuthModal';
import RegisterModal from './component/auth/RegisterModal';
import DocsPage from './component/document/DocsPage';
import DocPage from './component/document/DocPage';
import ReportDraftsPage from './component/reportDraft/ReportDraftsPage';
import ReportDraftPage from './component/reportDraft/ReportDraftPage';


export const AuthContext = createContext({
    isAuth: false,
    setAuth: () => {},
    user: {},
    setUser: () => {},
    isLoading: true,
    setLoading: () => {},
    isAuthModal: false,
    setAuthModal: () => {},
    isRegModal: false,
    setRegModal: () => {}
  })
  

export default function App() {
    const [isAuth, setAuth] = useState(false);
    const [user, setUser] = useState({
        firstname: "",
        lastname: "",
        login: ""
    })
    const [isAuthModal, setAuthModal] = useState(false);
    const [isRegModal, setRegModal] = useState(false);
    const [isLoading, setLoading] = useState(true)

    useEffect(() => {
  
        console.log(localStorage.getItem('token'))

                const fetchData = async() => {
                    if(localStorage.getItem('refresh_token')) {
                    setLoading(true)
                    console.log("App.jsx: Начал отправку рефреш запроса")
                    await axios.get(BASE_URL + '/auth/refresh-token', {
                        headers: {
                            Authorization: `Bearer ${localStorage.getItem('refresh_token')}`
                        }   
                    }).then((response) => {
                        const data = response.data;
                    
                        console.log("App.jsx: Я отправил рефреш запрос")
                        localStorage.setItem('token', data.access_token)
                        localStorage.setItem('refresh_token', data.refresh_token)
                        setAuth(true)
                        setUser(data.user)

                    })
                    
                }
                setLoading(false)
            }
                try{
                    fetchData()
                } catch(err) {
                    console.log(err)
                    localStorage.removeItem('token')
                }

    }, [])

    return (
        <AuthContext.Provider value={{isAuth, setAuth, user, setUser, isAuthModal, setAuthModal, isRegModal, setRegModal}}>
            {
                isLoading ? <div>Loading...</div> :
                <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<Root/>}>
                            <Route path='docs' element={
                                <ProtectedRoute>
                                    <DocsPage/>
                                </ProtectedRoute>
                            }>
                                <Route path=':docId' element={<DocPage/>}/>
                            </Route>
                            <Route path="projects" element={
                                <ProtectedRoute>
                                    <ProjectsPage/>
                                </ProtectedRoute>
                            }>
                                <Route path=":projectId" element={<SurveyPlacePage/>}/>
                            </Route>
                            <Route path="reports" element={
                                <ProtectedRoute>
                                    <ReportDraftsPage/>
                                </ProtectedRoute>
                            }>
                                <Route path=':reportDraftId' element={<ReportDraftPage/>}></Route>
                            </Route>
                            <Route path="*" element={<div>Такая страница не найдена</div>}/>
                            
                        </Route>
                    </Routes>
                </BrowserRouter>
            }
            
            <RegisterModal isModal={isRegModal} setModal={setRegModal}/>
            <AuthModal isModal={isAuthModal} setModal={setAuthModal}/>
            <ToastContainer
                position="bottom-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="dark"
            />  
        </AuthContext.Provider>
    )
}