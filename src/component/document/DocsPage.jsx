import { useEffect, useState } from "react"
import api from "../../api";
import { Outlet } from "react-router-dom";
import DocModal from "./DocModal";
import { Button } from "../common/Botton/Button";
import "./style.css"
import pencil from '../../img/pencil.svg'
import { NavBtn } from "../common/Botton/NavBtn";

export function Doc({doc}) {
    return( <div className='docElem'>
        <NavBtn to={`/docs/${doc.id}`} className='docBtn'>
            <div>{doc.title}</div> 
            <div>Автор: {doc.author.firstname} {doc.author.lastname}</div>
            </NavBtn>
        <img src={pencil}/>
    </div>)
}


export default function DocsPage() {
    const [isLoading, setLoading] = useState(true);
    const [docs, setDocs] = useState([])
    const [isModal, setModal] = useState(false);

    const fetchDocs = async () => {
        setLoading(true);
            const docs = (await api.get(`/docs`)).data
            console.log(docs)
            setDocs(docs.map(doc => {
                return {
                    id: doc.id,
                    title: doc.title,
                    description: doc.description,
                    abbreviation: doc.abbreviation,
                    accessType:doc.accessType,
                    author: {
                        id: doc.author.id,
                        firstname: doc.author.firstname,
                        lastname: doc.author.lastname
                    }
                }
            }))

        setLoading(false);
    }

    useEffect(() => {
        fetchDocs();
    }, [])

    useEffect(() => {
        if(!isModal)
            fetchDocs();
    }, [isModal])


    return (
    <div className='docPage'>
        <div className='sideBarContainer'>
            <div className='docsContainer'>{!isLoading ? <>{
                docs.length > 0 ?
                docs.map(doc => {
                    return (
                        <Doc key={doc.id} doc={doc} />
                        )
                    })
                : <div>Создай первый документ</div>}</>
                : <div>Loading...</div>
                }
            </div>
            <div className='addBtnContainer'>
                    <Button className='addBtn' onClick={() => setModal(true)}><b>Добавить документ</b></Button>
            </div>
            <DocModal isModal={isModal} setModal={setModal} isCreate={true}/>
            
        </div>
        <Outlet/>
    </div>
    )
}