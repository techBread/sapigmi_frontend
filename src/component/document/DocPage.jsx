import { createContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import api from "../../api";
import { NewSampleItem, SampleItem } from "../surveyPlace/SampleItems";
import { SelectItemsContext, loadMeasures } from "../surveyPlace/surveyPlacePage";
import { toast } from "react-toastify";

const WaterSampleItem = ({sample,objToAddId, setNeedReload, removeFunction}) => {
    return (
        <SampleItem sampl={sample} setNeedReload={setNeedReload} objToAddId={objToAddId} removeFunction={removeFunction}/>
    )
}

export default function DocPage() {
    const {docId} = useParams();
    const [isLoading, setLoading] = useState(true);
    const [doc, setDoc] = useState({})
    const [samples, setSamples] = useState([]);
    const [isNeedReload, setNeedReload] = useState(false)
    const [measures, setMeasures] = useState([])
    
    const fetchDoc = async (docId) => {
        return (await api.get(`docs/${docId}`)).data
    }

    const fetchSamples = async (docId) => { 
        return (await api.get(`docs/${docId}/samples`)).data
    }
    const fetchData = async () => {
        setLoading(true);
        setDoc(await fetchDoc(docId))
        setSamples(await fetchSamples(docId));
        setMeasures(await loadMeasures());
        setLoading(false);
    }
    useEffect(() => {
        fetchData();
    }, [])
    useEffect(() => {
        if(isNeedReload) {
            fetchData();
            setNeedReload(false)
        }
            
    }, [isNeedReload])

    const addSampleToDoc = async(docId, sample) => {
        return await api.post(`docs/${docId}/samples`, sample);
    }

    const removeSampleInDoc = async(docId, sampleId) => {
        return await api.delete(`docs/${docId}/samples/${sampleId}`)
    }


    return (
    <SelectItemsContext.Provider value={{valueMeasures: measures}} >
        <div className="sampleContainer">
            <ul className="sampleList">
            <NewSampleItem key={0} objToAddId={docId} setNeedReload={setNeedReload} addFunction={addSampleToDoc} />
            {isLoading ? <div>Loading...</div> :
                samples.map(sample => <WaterSampleItem sample={sample} setNeedReload={setNeedReload} objToAddId={docId} removeFunction={removeSampleInDoc}/>)
            }
            </ul>
            
        </div>
    </SelectItemsContext.Provider>
    )
}