import { useContext, useState } from "react";
import { Modal } from "../common/Modal/Modal";
import { AuthContext } from "../../App";
import { Button } from "../common/Botton/Button";
import api from "../../api";
import "./style.css"

export function DocForm({doc, setDoc}) {

    const changeInputDoc = event => {
        event.persist()
        setDoc(prev => {
            return {
                ...prev,
                [event.target.name]: event.target.value,
            }
        })
    }

    return (<form className="docForm">
        <input id="title"
            name="title"
            type="text"
            className="docInput"
            placeholder="Название..."
            onChange={changeInputDoc}
        />
        <input id="abbreviation"
            name="abbreviation"
            type="text"
            className="docInput"
            placeholder="Аббревиатура..."
            onChange={changeInputDoc}
        />
        <textarea id="description"
            name="description"
            type="text"
            className="docInput"
            placeholder="Описание..."
            onChange={changeInputDoc}
        />
        <input id="accessType"
            name="accessType"
            type="hidden"
            className="docInput"
            placeholder="Название..."
            onChange={changeInputDoc}
        />
    </form>)
}



export default function DocModal({isModal, setModal, isCreate}) {
    const [btnEnabled, setBtnEnabled] = useState(true);
    const {user} = useContext(AuthContext);
    const [doc, setDoc] = useState( {
        title: "",
        description: "",
        abbreviation: "",
        accessType: "PUBLIC",
        sampleType: "WATER",
        author: {...user}
    })

    const saveDoc = async() => {
        console.log(doc)
        setBtnEnabled(false);
        await api.post("/docs", {
            id: doc.id,
            title: doc.title,
            description: doc.description,
            abbreviation: doc.abbreviation,
            accessType:doc.accessType,
            sampleType:doc.sampleType,
            author: {
                id: doc.author.id,
                firstname: doc.author.firstname,
                lastname: doc.author.lastname
            }
        })
            .then()
            .catch()
        setModal(false);
        setBtnEnabled(true);
    }

    return ( <Modal
        isVisible={isModal}
        title={isCreate ? "Создание документа" : "Редактирование документа"}
        content={<DocForm doc={doc} setDoc={setDoc}/>}
        footer={<><Button onClick={() => {saveDoc()}} disabled={!btnEnabled}>Создать</Button><Button onClick={() => setModal(false)}>Отмена</Button></>}
        onClose={() => setModal(false)}
      />)
}