import { NavLink } from "react-router-dom"



export const NavBtn = ({to, children, disabled=false, style, className}) => {
    return (
        <NavLink to={to} className={className ? className : "button"} style={style}>{children}</NavLink>
    )
}