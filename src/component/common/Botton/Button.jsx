import "./style.css"
var classNames = require('classnames');


export const Button = ({onClick, children, disabled=false, style, className}) => {
    return (
        <button className={classNames({className, 'button':true})} onClick={onClick} style={style} disabled={disabled}>{children}</button>
    )
}