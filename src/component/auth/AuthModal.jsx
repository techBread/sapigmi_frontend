import { useContext, useState } from "react"
import { Button } from "../common/Botton/Button"
import { Modal } from "../common/Modal/Modal"
import api from "../../api"
import {toast} from 'react-toastify'
import { AuthContext } from "../../App"


const AuthForm = ({auth, setAuth}) => {

    const changeInputAuth = event => {
        event.persist()
        setAuth(prev => {
            return {
                ...prev,
                [event.target.name]: event.target.value,
            }
        })
    }
    return (
            <form>
                <p>Логин: <input 
                            type="username" 
                            id="login" 
                            name="login" 
                            value={auth.login}
                            placeholder="Введите логин..." 
                            onChange={changeInputAuth}/></p>
                <p>Пароль: <input 
                                type="password" 
                                id="password" 
                                name="password" 
                                value={auth.password} 
                                onChange={changeInputAuth}/></p>
            </form>
    )
}



export default function AuthModal({isModal, setModal}) {
    const [authe, setAuthe] = useState(() => {
        return {
            login: "",
            password: ""
        }
    })

    const [btnEnabled, setBtnEnabled] = useState(false);
    const {setAuth, setUser} = useContext(AuthContext);

    const AuthClick = async () => {
        setBtnEnabled(true);
        console.log(authe);
        try {
            const response = await api.post('/auth/authenticate', authe)
            localStorage.setItem('token', response.data.access_token)
            console.log(response.data)
            localStorage.setItem('refresh_token', response.data.refresh_token)
            setUser(response.data.user)
            toast.success(`Рады Вас сново видеть, ${response.data.user.firstname}!!!`)
        } catch (error) {
            toast.error(error.message)
            setBtnEnabled(false);
            return
        }
        setBtnEnabled(false);
        setAuth(true)
        setModal(false);
    }

    return (
        <Modal
          isVisible={isModal}
          title="Авторизоваться"
          content={<AuthForm auth={authe} setAuth={setAuthe}/>}
          footer={<><Button onClick={() => AuthClick()} disabled={btnEnabled}>Войти</Button><Button onClick={() => setModal(false)}>Отмена</Button></>}
          onClose={() => setModal(false)}
        />
    )
}