import { useContext, useState } from "react"
import { AuthContext } from "../../App"
import { Modal } from "../common/Modal/Modal"
import { Button } from "../common/Botton/Button"
import "./style.css"
import api from "../../api"
import { toast } from "react-toastify"
var classNames = require('classnames');



const RegForm = ({regData, setRegData}) => {
  const changeInputReg = event => {
    event.persist()
    setRegData(prev => {
      return {
        ...prev, 
        [event.target.name]: event.target.value
      }
    })
  }


  return (
      <form className="registrationForm">
        <input type="text"
                id="firstname"
                name="firstname"
                value={regData.firstname}
                placeholder="Введите ваше имя..."
                onChange={changeInputReg}
                />
        <input type="text"
                id="lastname"
                name="lastname"
                value={regData.secondname}
                placeholder="Введите вашу фамилию"
                onChange={changeInputReg}
                />
        <input type="username" 
                id="login"
                name="login"
                value={regData.login}
                placeholder="Введите логин..."
                onChange={changeInputReg}
                />
        <input type="password"
                id="password"
                name="password"
                value={regData.password}
                placeholder="Введите пароль..."
                onChange={changeInputReg}
                />
        <input type="password"
                id="passwordConfirm"
                name="passwordConfirm"
                value={regData.password}
                placeholder="Повторите ввод пароля..."
                onChange={changeInputReg}
                />
      </form>
  )

}





export default function RegisterModal({setModal, isModal}) {
  const [regData, setRegData] = useState(() => {
    return {
      firstname: "",
      lastname: "",
      login: "",
      password: "",
      confirmPass: "",
    }
  })

  const [btnEnabled, setBtnEnabled] = useState(true);
  const {setAuth, setUser} = useContext(AuthContext)

  const regClick = async () => {
    setBtnEnabled(true)
    console.log(regData)
    try {
      const removeConfirmPass = ({confirmPass, ...regData}) => regData
      console.log(removeConfirmPass(regData))
      var response = await api.post(`/auth/register`, removeConfirmPass(regData))
      localStorage.setItem('token', response.data.access_token)
      console.log(response.data)
      localStorage.setItem('refresh_token', response.data.refresh_token)
      setUser(response.data.user)
      toast.success(`Добро пожаловать, ${response.data.user.firstname}`)
    } catch (error) {
      toast.error(error.message)
      setBtnEnabled(false);
      return
    }

    setBtnEnabled(false)
    setAuth(true)
    setModal(false)
  }

    return(
        <Modal
        isVisible={isModal}
        title="Регистрация"
        content={<RegForm regData={regData} setRegData={setRegData}/>}
        footer={<>
            <Button onClick={() => regClick()} disabled={!btnEnabled}>Регистрация</Button>
            <Button onClick={() => setModal(false)}>Отмена</Button>
          </>}
        onClose={() => setModal(false)}
      />
    )
}