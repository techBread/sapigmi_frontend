import { Navigate, useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import AuthModal from "../auth/AuthModal";
import { useContext } from "react";
import { AuthContext } from "../../App";

export default function ProtectedRoute({
    children,
  }) {
    const location = useLocation();
    const {isAuth} = useContext(AuthContext)
    const {isAuthModal, setAuthModal} = useContext(AuthContext);
    console.log(isAuth)
    if (!isAuth) {
      toast.warning("Необходимо авторизоваться!")
      setAuthModal(true);
      return <div>Ожидание авторизации...</div>
    }

    return children;
  };