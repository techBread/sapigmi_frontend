import { NavLink } from "react-router-dom"
import './style.css';
import { Button } from "../../common/Botton/Button";
import { useContext, useState } from "react";
import RegisterModal from "../../auth/RegisterModal";
import AuthModal from "../../auth/AuthModal";
import { AuthContext } from "../../../App";
import { NavBtn } from "../../common/Botton/NavBtn";

export const Navbar = (props) => {


    const {isAuth, user, setAuthModal, setRegModal} = useContext(AuthContext);
    return (
        <>
            <nav className="nav">
            <div className="container">
                <div className="nav-row">
                    <NavLink  to="/" className="logo">
                        <strong>СапИгми</strong>
                    </NavLink>
                    {!isAuth ? 
                    <div className="buttonContainer">
                    <Button style={{width: 8 + "vw"}} onClick={() => setAuthModal(true)}>
                        Вход
                    </Button>
                    <Button style={{width: 12 + "vw"}} onClick={() => setRegModal(true)}>
                        Регистрация
                    </Button>
                    </div> : 
                    <div className="buttonContainer">
                        <NavLink to="/reports" style={{width:10+"vw"}} className={"button"}>Отчеты</NavLink>
                        <NavLink to="/docs" style={{width:10+"vw"}} className={"button"}>Документы</NavLink> 
                        <NavLink  to="/projects" style={{width: 10 + "vw"}} className={"button"}>Проекты</NavLink>
                        <Button style={{width: 15 + "vw"}}>{user.firstname + " " + user.lastname}</Button>
                    </div>
                    }
                    
                </div>
            </div>
            </nav>
        </>
        
        
    )
}