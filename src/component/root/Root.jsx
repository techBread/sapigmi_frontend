import { Outlet } from "react-router-dom";
import { Navbar } from "./Navbar/Navbar";
import 'react-toastify/dist/ReactToastify.css'
import './style.css'

export default function Root() {
    return (
            <div style={{height: '100vh'}}>
                <Navbar/>
                <Outlet/>
            </div> 
        );
}