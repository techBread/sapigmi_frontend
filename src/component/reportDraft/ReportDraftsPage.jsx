import { useEffect, useState } from "react"
import api from "../../api";
import { Outlet } from "react-router-dom";
import { Button } from "../common/Botton/Button";
import "./style.css"
import pencil from '../../img/pencil.svg'
import { NavBtn } from "../common/Botton/NavBtn";
import { ReportDraftModal } from "./ReportDraftModal";

export function ReportDraft({reportDraft}) {
    return( <div className='reportDraftElem'>
        <NavBtn to={`/reports/${reportDraft.id}`} className='reportDraftBtn'>
            <div>{reportDraft.title}</div> 
            </NavBtn>
        <img src={pencil}/>
    </div>)
}


export default function ReportDraftsPage() {
    const [isLoading, setLoading] = useState(true);
    const [reportDrafts, setReportDrafts] = useState([])
    const [isModal, setModal] = useState(false);

    const fetchReportDrafts = async () => {
        setLoading(true);
            const reportDrafts = (await api.get(`/report-draft`)).data
            console.log(reportDrafts)
            setReportDrafts(reportDrafts.map(reportDraft => {
                return {
                    id: reportDraft.id,
                    title: reportDraft.title,
                    description: reportDraft.description,
                }
            }))
        setLoading(false);
    }

    useEffect(() => {
        fetchReportDrafts();
    }, [])

    useEffect(() => {
        if(!isModal)
            fetchReportDrafts();
    }, [isModal])


    return (
    <div className='reportDraftPage'>
        <div className='sideBarContainer'>
            <div className='reportDraftsContainer'>{!isLoading ? <>{
                reportDrafts.length > 0 ?
                reportDrafts.map(reportDraft => {
                    return (
                        <ReportDraft key={reportDraft.id} reportDraft={reportDraft} />
                        )
                    })
                : <div>Создай первый шаблон отчета</div>}</>
                : <div>Loading...</div>
                }
            </div>
            <div className='addBtnContainer'>
                    <Button className='addBtn' onClick={() => setModal(true)}><b>Добавить шаблон отчета</b></Button>
            </div>
        </div>
        <ReportDraftModal isModal={isModal} setModal={setModal} isCreate={true}/>
        <Outlet/>
    </div>
    )
}