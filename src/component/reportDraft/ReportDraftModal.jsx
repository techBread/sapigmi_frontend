import { useContext, useState } from "react";
import { Modal } from "../common/Modal/Modal";
import { AuthContext } from "../../App";
import { Button } from "../common/Botton/Button";
import api from "../../api";
import "./style.css"
import { toast } from "react-toastify";

export function ReportDraftForm({reportDraft, setReportDraft}) {

    const changeInputReportDraft = event => {
        event.persist()
        setReportDraft(prev => {
            return {
                ...prev,
                [event.target.name]: event.target.value,
            }
        })
    }

    return (
    <form className="reportDraftForm">
        <input id="title"
            name="title"
            type="text"
            className="reportDraftInput"
            placeholder="Название..."
            onChange={changeInputReportDraft}
        />
        <textarea id="description"
            name="description"
            type="text"
            className="reportDraftInput"
            placeholder="Описание..."
            onChange={changeInputReportDraft}
        />
    </form>)
}



export function ReportDraftModal({isModal, setModal, isCreate}) {
    const [btnEnabled, setBtnEnabled] = useState(true);
    const [reportDraft, setReportDraft] = useState( {
        title: "",
        description: "",
    })

    const saveReportDraft = async() => {
        console.log(reportDraft)
        setBtnEnabled(false);
        await api.post("/report-draft", {
            title: reportDraft.title,
            description: reportDraft.description,
        })
            .then((resp) => {
                if(resp.status == 201)
                {
                    toast.success(<div>Шаблон с именем {reportDraft.title} успешно добавлен</div>)
                }
                else 
                {
                    toast.error(<div>Шаблон с именем {reportDraft.title} небыл добавлен:
                        Ошибка: {resp.data.message}</div>)
                }
                    
            })
            .catch((error) => toast.error(<div>Шаблон с именем {reportDraft.title} небыл добавлен:
            Ошибка: {error.message}</div>))
        setModal(false);
        setBtnEnabled(true);
    }

    return ( <Modal
        isVisible={isModal}
        title={isCreate ? "Создание шаблона отчета" : "Редактирование шаблона отчета"}
        content={<ReportDraftForm reportDraft={reportDraft} setReportDraft={setReportDraft}/>}
        footer={<><Button onClick={() => {saveReportDraft()}} disabled={!btnEnabled}>Создать шаблон</Button><Button onClick={() => setModal(false)}>Отмена</Button></>}
        onClose={() => setModal(false)}
      />)
}