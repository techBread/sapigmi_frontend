import DraftDocuments from "./DraftDocuments";
import DraftProps from "./DraftProps";
import DraftSurveyPlaceTypes from "./DraftSurveyPlaceTypes";



export default function DraftRequiredData() {
    return(
    <>
            <DraftDocuments/>
            <DraftSurveyPlaceTypes/>
            <DraftProps/>
    </>
    )
}