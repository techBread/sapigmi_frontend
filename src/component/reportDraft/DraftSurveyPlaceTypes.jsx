import { useContext, useState } from "react";
import { ReportContext } from "./ReportDraftPage";
import arrow from "../../img/arrow-down-svgrepo-com.svg"

export default function DraftSurveyPlaceTypes() {
    const {report} = useContext(ReportContext)
    const [isOpen, setOpen] = useState(false);

    return (
        <div className="draftInfoContainer">
            <div className="draftRedirectInfo">
                <div className="infoName">Виды мест изыскания</div>
                <div className="infoDataCount">
                    Количество видов мест изыскания: {report.draft.surveyPlaceTypes.length}<br/>
                    Количество ненайденых видов: {report.draft.surveyPlaceTypes.filter(type => !type.className).length}
                </div>
                <img src={arrow} className="arrowDownImg" onClick={() => {setOpen(!isOpen)}}/>
            </div>
            {isOpen && 
            <div className="openDraftInfo">
                text
            </div>} 
        </div>
    )
}