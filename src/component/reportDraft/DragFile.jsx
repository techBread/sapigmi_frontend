import { useRef, useState } from "react";
import "./style.css"
import { toast } from "react-toastify";
import api from "../../api";

export default function DragFile({file, setFile}) {
    const [dragActive, setDragActive] = useState(false);

    const inputRef = useRef(null);


    const handleFile = (file) => {
        var ext = "не определилось"
        var parts = file.name.split('.');
        if (parts.length > 1) ext = parts.pop();
        if(ext === "docx") {
            
            setFile(file);
            toast.success(
            <div>
                Файл: {file.name}<br/>
                Размер: {file.size}<br/>  
                Успешно загружен
            </div>
            );
        } else {
            toast.warning(<div>
                Файл: {file.name}<br/>
                Имеет недопустимое расширение
            </div>)
        }
       
    }

    const handleDrag = function(e) {
        e.preventDefault();
        e.stopPropagation();
        if (e.type === "dragenter" || e.type === "dragover") {
          setDragActive(true);
        } else if (e.type === "dragleave") {
          setDragActive(false);
        }
      };

        // triggers when file is dropped
    const handleDrop = function(e) {
        e.preventDefault();
        e.stopPropagation();
        setDragActive(false);
        if (e.dataTransfer.files && e.dataTransfer.files[0]) {
            handleFile(e.dataTransfer.files[0]);
        }
    };

      // triggers when file is selected with click
    const handleChange = function(e) {
        e.preventDefault();
        if (e.target.files && e.target.files[0]) {
            handleFile(e.target.files[0]);
        }
    };

    // triggers the input when the button is clicked
    const onButtonClick = () => {
        inputRef.current.click();
    };
    return (        
    <form id="form-file-upload" onDragEnter={handleDrag} onSubmit={(e) => e.preventDefault()}>
        <input ref={inputRef} type="file" id="input-file-upload" accept=".docx" multiple={false} onChange={handleChange} />
        <label id="label-file-upload" htmlFor="input-file-upload" className={dragActive ? "drag-active" : "" }>
        <div>
            {}
            <div>Вставте шаблон здесь или</div>
            <button className="upload-button" onClick={onButtonClick}>нажмите для открытия окна выбора</button>
        </div> 
        </label>
        { dragActive && <div id="drag-file-element" onDragEnter={handleDrag} onDragLeave={handleDrag} onDragOver={handleDrag} onDrop={handleDrop}></div> }
    </form>
  )

}