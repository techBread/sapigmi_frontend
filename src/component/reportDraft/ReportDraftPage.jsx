import { createContext, useContext, useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import "./style.css"
import DragFile from "./DragFile";
import { Button } from "../common/Botton/Button";
import api from "../../api";
import { toast } from "react-toastify";
import { timeConverter } from "../../function/DateFunction";
import DraftRequiredData from "./DraftRequiredData";

export const ReportContext = createContext({
    report: {},
    setReport: () => {}
})


export default function ReportDraftPage() {
    const {reportDraftId} = useParams();
    const [isLoading, setLoading] = useState(true);
    const [report, setReport] = useState({});
    const [file, setFile] = useState(null);

    const sendFile = async() => {
        const formData = new FormData();
        formData.append('file', file);
        await api.post(`report-draft/${reportDraftId}`, formData, {
            headers: {
                "Content-type": "multipart/form-data"
            },
        }).then(res => {
            if(res.status === 200)
            {
                toast.success(<div>Файл <b>{file.name}</b> успешно добавлен на сервер</div>)
            }
               
        }).catch(err => {
            console.log(err);
        })
        await fetchServerDraftInfo();
    }

    const fetchServerDraftInfo = async () => {
        setLoading(true);
        try {
            const report = (await api.get(`report-draft/${reportDraftId}`)).data
            setReport(report);
        } catch {
            toast.error(<div>Неполучилось загрузить шаблон документа</div>);
        }
        setLoading(false);
    }

    useEffect(() => {
        setReport({})
        fetchServerDraftInfo();
    }, [reportDraftId])

    useEffect(() => {
        fetchServerDraftInfo();
    }, [])

    return (
        <ReportContext.Provider value={{report, setReport}}>
            <div className="reportDraft">
                <div className="fileInfoContainer">
                <div className="dropFileContainer">
                    <DragFile file={file} setFile={setFile}/>
                    {file && <div>Текущий загруженный файл: <b>{file.name}</b></div>}
                    <Button onClick={() => {sendFile()}}>Отправить файл на сервер</Button>
                </div>
                <div className="serverFileContainer">
                    {!isLoading ? 
                        report.draft ? 
                        <div>
                            <div>
                                Файл загруженный на сервер: {report.draft.name}<br/>
                                Время загрузки файла: {timeConverter(report.draft.loadDate) }
                            </div>
                        </div> 
                        :
                        <div>Нет файла на сервере</div>
                        : 
                        <div>Загрузка информации с сервера...</div>
                    }
                </div>
                </div>
                <div className="draftRequiredDataContainer">
                    {report.draft ? 
                        <DraftRequiredData/>
                    : 
                        <div>Нет данных</div>
                    }
                </div>
            </div>
        </ReportContext.Provider>
 
    
    );

}