import { useContext, useState } from "react"
import saveImg from "../../img/save-item.svg"
import { parse } from "../../function/DateFunction";

import Select, { components, MenuListProps } from 'react-select';
import api from "../../api";
import {cloneDeep} from 'lodash'
import { Modal } from "../common/Modal/Modal";
import { toast } from "react-toastify";
import { ReportContext } from "./ReportDraftPage";

const hazardClasses = ["Нет значения", "4 - мало опастное", "3 - умерено опастное", "2 - высоко опастное", "1 - чрезвычайно опасное"]



const PropForm = ({prop, setProp, setBtnEnabled}) => {
    const changeProp = event => {
        event.persist()
        setProp(prev => {
          return {
            ...prev, 
            [event.target.name]: event.target.value
          }
        })
      }

    return(
    <form className="propForm">
        <div className="requiredField">
            {!prop.formula && <span>*обязательное поле</span>}
            {setBtnEnabled(prop.formula)}
            <input type="text"
                id="formula"
                name="formula"
                value={prop.formula}
                placeholder="Введите формулу..."
                onChange={changeProp}
            />
        </div>
       
        <input type="text"
            id="NCAS"
            name="NCAS"
            value={prop.NCAS}
            placeholder="Введите NCAS..."
            onChange={changeProp}
        />
        <div id="hazardInput">
            <span>
                Класс опасности
            </span>
            <div id="hazardValueContainer">
                <input type="range"
                    id="hazardClass"
                    name="hazardClass"
                    min={1} step={1} max={5}
                    value={prop.hazardClass}
                    placeholder="Введите NCAS..."
                    onChange={changeProp}
                />
                <span>
                    {hazardClasses[5 - prop.hazardClass]}
                </span>
            </div>
            
        </div>
        <input type="text"
            id="limitIndicatorHarmfulness"
            name="limitIndicatorHarmfulness"
            value={prop.limitIndicatorHarmfulness}
            placeholder="Введите лимитирующий показатель вредности..."
            onChange={changeProp}
        />
    </form>
    )


}


export const NewPropertyModal = ({isModal, setModal}) => {
    const [prop, setProp] = useState({
        hazardClass:5
    }) 
    const [btnEnabled, setBtnEnabled] = useState(true);

    const postProp = async () => {
        setBtnEnabled(false);
        try {
            const res = await api.post(`chem-property`, prop)
            console.log(res)
            toast.success(<div>Свойсво {parse(prop.formula)} успешно добавлено</div>)
        } catch {
            toast.error(<div>Не удалось добавить {parse(prop.formula)}</div>)
        }
        setBtnEnabled(true);
        setModal(false);
    }

    return(
        <Modal
            isVisible={isModal}
            title={"Создание свойства"}
            content={
                <PropForm
                   prop={prop}
                   setProp={setProp} 
                   setBtnEnabled={setBtnEnabled}
                />}
            footer={
                <>
                    <button 
                        className="propBtn" 
                        onClick={() => postProp()} 
                        disabled={!btnEnabled}>
                            Создать
                        </button>
                        <button 
                            className="propBtn" 
                            onClick={() => setModal(false)}>
                                Отмена
                        </button></>}
            onClose={() => setModal(false)}
        />
    )
}


export default function PropertyNameItem({property}) {
    const {report} = useContext(ReportContext);
    const [prop, setProp] = useState(cloneDeep(property));
    const [isEnable, setEnable] = useState(true);
    const [isModal, setModal] = useState(false);
    const [propOptionData, setOptionData] = useState({
        propOptions: [],
        isLoading: false,
        inputValue: ""
    })

    const changeProp = event => {
        event.persist()
        setProp(prev => {
          return {
            ...prev, 
            [event.target.name]: event.target.value
          }
        })
      }
    const changeOptionData = (field, value) => {
        setOptionData(prev => {
            return {
                ...prev,
                [field]: value
            }
        })
    }
    
    const fetchProps = async () => {
        changeOptionData("isLoading", true)
        const res = await api.get(`chem-property`, {params: 
            {"chem-name": propOptionData.inputValue, 
                "draft-file-id": report.draft.id
            }}).then(resp => {
                changeOptionData("propOptions", resp.data.map(p => {return {value: p, label: p.formula}}))
            })
        changeOptionData("isLoading", false)
    }

    const changeInputValue = async ({inp}) => {
        changeOptionData("inputValue", inp)
        await fetchProps();
    }

    const updateProp = async () => {
        setEnable(false)
        const resp = await api.put(`chem-property/names/${prop.id}`, prop)
        if(resp.status == 200) {
            toast.success(<div>Свойсво документа {prop.shortName} обновлено</div>)
        } else {
            toast.warning(<div>Свойсво документа {prop.shortName} не обновлено</div>)
        }
        setEnable(true)
    }

    const MenuList = (
        props
      ) => {
        return (
          <components.MenuList {...props}>
            <button className="propBtn" onClick={() => {setModal(true)}}>Добавить свойсво</button>
            {props.children}
          </components.MenuList>
        );
    };

    return (
        <li key={prop.id} className="propertyNameItem">
            <form className="propForm">
                <input type="text"
                    id="name"
                    name="name"
                    value={prop.name}
                    placeholder="Введите название..."
                    onChange={changeProp}
                    isEnable={isEnable}
                    />
                <input type="text"
                    id="shortName"
                    name="shortName"
                    value={prop.shortName}
                    placeholder="Введите короткое название..."
                    onChange={changeProp}
                    isEnable={isEnable}
                    />
                    <Select
                        id="property"
                        className="propSelectInput"
                        placeholder="Выбери свойство"
                        onMenuOpen={fetchProps}
                        onInputChange={inp => {changeInputValue(inp)}}
                        isLoading={propOptionData.isLoading}
                        options={propOptionData.propOptions}  
                        formatOptionLabel={({value, label}) => <>{parse(label)}</>}
                        onChange={(p) =>{let _prop = {...prop}; _prop.property = p.value; setProp(_prop)}}
                        defaultValue={{value: prop.property, label: prop.property ? prop.property.formula : ""}}
                        isEnable={isEnable}
                        components={{MenuList}}
                    />
                
            </form>
            <img src={saveImg} className="saveImg" onClick={updateProp}/>
            <NewPropertyModal isModal={isModal} setModal={setModal}/>
        </li>
    )
}