import { useContext, useState } from "react";
import { ReportContext } from "./ReportDraftPage";
import arrow from "../../img/arrow-down-svgrepo-com.svg"
import PropertyNameItem from "./PropertyNameItem";

export default function DraftProps() {
    const {report} = useContext(ReportContext)
    const [isOpen, setOpen] = useState(false);
    

    return (
        <div className="draftInfoContainer">
            <div className="draftRedirectInfo">
                <div className="infoName">Названия свойств</div>
                <div className="infoDataCount">
                    Количество названий свойств: {report.draft.propertyNames.length}<br/>
                    Количество ненайденых названий: {report.draft.propertyNames.filter(type => !type.property).length}
                </div>
                <img src={arrow} className="arrowDownImg" onClick={() => {setOpen(!isOpen)}}/>
            </div>
            {isOpen && 
            <ul className="openDraftInfo">
                {report.draft.propertyNames.sort(
                    (a, b) => {if(a.property && b.property) {
                        return a.shortName>b.shortName;
                    } else if(!a.property && !b.property) {
                        return a.shortName>b.shortName;
                    } else if(!a.property && b.property) {
                        return 6 + a.shortName>b.shortName
                    } else {
                        return 3 + a.shortName<b.shortName
                    }}
                ).map(property => <PropertyNameItem property={property}/>)}
            </ul>}  
        </div>
    )
}