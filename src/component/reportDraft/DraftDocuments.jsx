import { useContext, useState } from "react"
import { ReportContext } from "./ReportDraftPage"
import arrow from "../../img/arrow-down-svgrepo-com.svg"

export default function DraftDocuments() {
    const {report} = useContext(ReportContext);
    const [isOpen, setOpen] = useState(false);

    return (
        <div className="draftInfoContainer">
            <div className="draftRedirectInfo">
                <div className="infoName">Документы отчета</div>
                <div className="infoDataCount">
                    Количество документов: {report.draft.docs.length}<br/>
                    Количество ненайденых документов: {report.draft.docs.filter(doc => !doc.title).length}
                </div>
                <img src={arrow} className="arrowDownImg" onClick={() => {setOpen(!isOpen)}}/>
            </div>
            {isOpen && 
            <div className="openDraftInfo">
                text
            </div>} 
        </div>
        
    )
}