import { useEffect, useState, CSSProperties} from "react";
import { useParams } from "react-router-dom"
import api from "../../api";
import "./style.css"
import plus from "../../img/plus-svgrepo-com.svg"
import Select from 'react-select';
import { toast } from "react-toastify";

export const NewPlaceItem = ({key,placeTypes, setNeedReload}) => {
    const {projectId} = useParams();
    const [place, setPlace] = useState({
        title: "",
        description: "",
        type: placeTypes[0].options[0].value
    })
    const [enableAddBtn, setEnable] = useState(true)

    const changePlaceData = event => {
        event.persist()
        setPlace(prev => {
            return {
                ...prev,
                [event.target.name]: event.target.value,
            }
        })
    }

    const btnAddClick = async() => {
        if(enableAddBtn) {
            setEnable(false);
            console.log(place)
            if(enableAddBtn)
                if(place.title.length > 5 && place.title.length < 64 && place.description.length < 1024)
                try {
                    if((await api.post(`/projects/${projectId}/survey-places`, place)).status === 201) {
                        toast.success(`Место изысканий ${place.title} успешно создано`);
                        setNeedReload(true)
                    }
                }   catch(error) {
                    toast.error(error.message)
                }
               else {
                toast.warning(<><p>Длина названия: от 6 до 64</p>
                <p>Длина описания: до 1024</p></>)
               } 
            setEnable(true);
        }
       
    }

    return(
        <form key={key} className="placeLi">
                <div className="placeContainer">
                    <div className="placeInfoContainer">
                        <input type="text" 
                            name="title"
                            className="placeTitleInput placeTitle" 
                            placeholder="Название..."
                            value={place.title}
                            onChange={changePlaceData}
                        />
                        <textarea type="text" 
                            name="description"
                            className="placeDescriptionTextArea placeDescription" 
                            placeholder="Описание..."
                            value={place.description}
                            onChange={changePlaceData}
                        />
                    </div>
                    <div className="placeTypeContainer">
                        <Select
                            className="placeType"
                            options={placeTypes}
                            isDisabled={false}
                            defaultValue={placeTypes[0].options[0]}
                            onChange={(p) =>{let _place = {...place}; _place.type = p.value; setPlace(_place)}}
                        />
                    </div>
                    <img className="plusImg" src={plus} onClick={btnAddClick}/>
                </div>
        </form>
    )
}