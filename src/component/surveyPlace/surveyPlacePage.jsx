import { useEffect, useState, CSSProperties, createContext} from "react";
import { useParams } from "react-router-dom"
import api from "../../api";
import "./style.css"
import downArrow from "../../img/arrow-down-svgrepo-com.svg"
import Select from 'react-select';
import { groupBy } from "lodash";
import { NewPlaceItem } from "./NewPlaceItem";
import {NewSampleItem, SampleItem} from "./SampleItems";
import { Link, animateScroll as scroll } from "react-scroll";


const typesToOptional = (types) => {
    var classes = groupBy(types, "typeClass");
    
    var groupOptions = Object.keys(classes).map(key => {
        return {label: key, options: classes[key].map(type => {
            return {label: type.type, value: type}
        })}
    })
    console.log(groupOptions);

    return groupOptions;
}

const measureToOptional = (measures) => {
    var classes = groupBy(measures, "type.name");
    
    var groupOptions = Object.keys(classes).map(key => {
        return {label: key, options: classes[key].map(measure => {
            return {label: measure.currency , value: measure}
        })}
    })
    console.log(groupOptions);

    return groupOptions;
}

export const loadMeasures = async (type) => {
    var measures = typeof type === "undefined" ? (await api.get(`measures`)).data : (await api.get(`measures`), {params: { typeId: type.id } }).data
    console.log(measures)
    return measureToOptional(measures);
}

const loadPlaceTypes = async () => {
    var types = (await api.get(`/survey-place/types`)).data
    console.log(typesToOptional(types))
    return typesToOptional(types)
}

export const SelectItemsContext = createContext({
    valueMeasures: [],
    setMeasures: () => {},
    placeTypes: [],
    setPlaceTypes: () => {}
})


export default function SurveyPlacePage() {
    const {projectId} = useParams();
    const [isLoading, setLoading] = useState(true);
    const [places, setPlaces] = useState([])
    const [placeTypes, setPlaceTypes] = useState([])
    const [valueMeasures, setMeasures] = useState([]);
    const [needReload, setNeedReload] = useState(true);
    
    const fetchPlaces = async () => {
        setLoading(true);
        const data = (await api.get(`/projects/${projectId}/survey-places`)).data
        setPlaces(data);
        setPlaceTypes(await loadPlaceTypes())
        setMeasures(await loadMeasures());
        setLoading(false);
    }   

    useEffect(() => {
        fetchPlaces();
    }, [projectId])

    useEffect(() => {
        if(needReload) {
            fetchPlaces();
            setNeedReload(false);
        }
    }, [needReload])

    return (
        <SelectItemsContext.Provider value={{valueMeasures, setMeasures, placeTypes, setPlaceTypes}}>
            <div className="surveyPlaceContainer">
                        {isLoading ? <div>Loading...</div> 
                        : 
                        <ul className="surveyPlaceList">
                            <NewPlaceItem key="0" placeTypes={placeTypes} setNeedReload={setNeedReload}/> 
                            {places.map(p => <PlaceItem placeId={p.id} placeTypes={placeTypes}/>)}
                        </ul>
                        }
            </div>
        </SelectItemsContext.Provider>
        
    )
}



const PlaceItem = ({placeId, placeTypes}) => {
    const [place, setPlace] = useState({})
    const [isOpen, setOpen] = useState(false);
    const [samples, setSamples] = useState([]);
    const [isLoadingPlace, setLoadingPlace] = useState(true)
    const [isLoading, setLoading] = useState(true);
    const [isNeedReload, setNeedReload] = useState(true);

    useEffect(() => {
        const fetchPlace = async () => {
            setLoadingPlace(true);
            const place = (await api.get(`survey-place/${placeId}`)).data
            console.log(place.type.type)
            setPlace(place)
            setLoadingPlace(false);
        }

        fetchPlace();
    }, [])

    const fetchSamples = async () => {
        setLoading(true);
        const samples = (await api.get(`survey-place/${placeId}`)).data.samples
        setSamples(samples);
        setNeedReload(false);
        setLoading(false);
    }

    useEffect(() => {
        if(isOpen) {
            fetchSamples();
        }
    }, [isOpen])

    useEffect(() => {
        if(isNeedReload) {
            fetchSamples();
        }
    }, [isNeedReload])

    const openPlace = () => {
        setOpen(!isOpen);
    }
    const addSampleToPlace = async (placeId, sample) => {
        return api.post(`/survey-place/${placeId}/sample`, sample)
    }
    return (<li key={place.id} className="placeLi">
        { !isLoadingPlace ?
                <div className="placeContainer">
                    <div className="placeInfoContainer">
                        <p className="placeTitle"><b>{place.title}</b></p>
                        <p className="placeDescription">{place.description}</p>
                    </div>
                    <div className="placeTypeContainer">
                        <Select
                            placeholder="Тип..."
                            options={placeTypes}
                            isDisabled={true}
                            defaultValue={typesToOptional([place.type])[0].options[0]}
                        />
                    </div>
                    <img className="arrowDownImg" src={downArrow} onClick={openPlace}/>
                </div>
                :
                <div>Loading...</div>
            }
        
        {isOpen && <ul className={`sampleList`}>
            <NewSampleItem objToAddId={place.id} setNeedReload={setNeedReload} addFunction={addSampleToPlace}/>
            { !isLoading ?
                samples.map(sample => <SampleItem sampl={sample}/>)
                :
                <div>Loading...</div>
            }
            </ul>}
    </li>)
}

