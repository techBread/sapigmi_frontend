import { useContext, useState } from 'react';
import Select from 'react-select';
import AsyncSelect from 'react-select/async';
import "./style.css";
import api from '../../api';
import { SelectItemsContext } from './surveyPlacePage';
import plus from "../../img/plus-svgrepo-com.svg";
import saveItem from "../../img/save-item.svg";
import deleteItem from "../../img/trash-2.svg"
import { toast } from 'react-toastify';


const parse = require('html-react-parser');


const sampleDataType = [
    {value: "doubleValue", label: "Числовой"},
    {value: "stringValue", label: "Строковый"}
]

const Prefixes = [
    {value: "EQUAL", label: "равно"},
    {value: "MORE", label: "более"},
    {value: "LESS", label: "менее"},
    {value: "NO_LESS", label: "не меньше"},
    {value: "NO_MORE", label: "не больше"},
    {value: "MORE_SIGN", label: ">"},
    {value: "LESS_SIGN", label: "<"},
]

const promiseOptions = (inputValue) =>
  new Promise((resolve) => {
    api.get(`chem-property`, {params: {"chem-name": inputValue}})
    .then(res => {
        resolve(res.data.map(u => {return {value: u, label: u.formula}}))
    })
    .catch(err => {
        //console.log(err)
        resolve([])})
  });

async function loadChemName(inputValue, callback) {
    api.get(`chem-property`, {params: {"chem-name": inputValue}})
    .then(res => {
        callback(res.data.map(u => {return {value: u, label: u.formula}}))
    })
    .catch(err => {
        //console.log(err)
        callback()})
}


const RealNumberForm = ({sample, setSample}) => {
    const {valueMeasures} = useContext(SelectItemsContext);

    return(
        <form className='realNumberForm'>
            <p>Значение: </p>
            <Select
                placeholder="Префикс..."
                className="prefixCb"
                classNamePrefix="value-cb"
                options={Prefixes}
                value={Prefixes.find((pref) => pref.value === sample.value.prefix)}
                onChange={(s) =>{let _sample = {...sample}; _sample.value.prefix = s.value; setSample(_sample)}}
            />
            <input type="number" 
                className='numInput'
                value={sample.value.value}
                onChange={(s) => {let _sample = {...sample}; _sample.value.value = s.target.value; setSample(_sample)}}
            />
            <Select
                placeholder="Измерение..."
                className="measureCb"
                classNamePrefix="value-cb"
                value={{label: sample.value.measure ? sample.value.measure.currency : "", value: sample.measure}}
                isClearable
                formatOptionLabel={({value, label}) => <>{parse(label)}</>}
                options={valueMeasures}
                onChange={(s) =>{let _sample = {...sample}; _sample.value.measure = s ? s.value : null ; setSample(_sample)}}
            />
        </form>
    ) 
}

const StringValueForm = ({sample, setSample}) => {
    return(
        <form className='realNumberForm'>
            <p>Значение: </p>
            <input type="text" 
                className='textInput'
                value={sample.value.value}
                onChange={(s) => {let _sample = {...sample}; _sample.value.value = s.target.value; setSample(_sample)}}
            />
        </form>
    ) 
}


export const NewSampleItem = ({objToAddId, setNeedReload, addFunction}) => {
    const [sample, setSample] = useState({
        class: "water",
        value: {
            value:0,
            class: sampleDataType[0].value,
            prefix: "EQUAL"
        },
        propertyName: {
        }
        })
    const [isEnable, setEnable] = useState(true)

    const addBtnClick = async () => {
        if(isEnable) {
            setEnable(false);
            if((await addFunction(objToAddId, sample)).status === 201){
                setNeedReload(true)
                toast.success(<div>Химическое свойство {parse(sample.property.formula)} успешно добавлено</div>);
            }

        }
        setEnable(true);
    }

    return (
        <form className='sampleItem'>
            <div className='sampleContainer'>
                <div className="sampleDataContainer">
                    <p>Тип значения: </p>
                    <Select  
                            className="typeValueComboBox"
                            classNamePrefix="value-cb"
                            defaultValue={sampleDataType.find(dataType => dataType.value === sample.value.class)}
                            name="sampleType"
                            options={sampleDataType}
                            onChange={(s) =>{let _sample = {...sample}; _sample.value.class = s ? s.value : sampleDataType[1]; setSample(_sample)}}
                        />
                    <p id="chemPropP">Химическое свойство:</p>
                    <AsyncSelect id="users" 
                        className="chemNameCb"
                        cacheOptions
                        placeholder="Химическое свойство..."
                        formatOptionLabel={({value, label}) => <>{parse(label)}</>}
                        value={sample.propertyName.formula}
                        onChange={(s) =>{let _sample = {...sample}; _sample.property = s.value; setSample(_sample)}}
                        loadOptions={promiseOptions} 
                        defaultOptions
                    />
                </div>
                <div className="valueDataContainer">
                    {sample.value.class === sampleDataType[1].value ? 
                        <StringValueForm sample={sample} setSample={setSample}/>
                     : 
                        <RealNumberForm sample={sample} setSample={setSample}/>
                    }
                </div>
            </div>
            <img className="plusImg" src={plus} onClick={addBtnClick}/>
        </form>
            
    )

}

export const SampleItem = ({sampl, setNeedReload, removeFunction, objToAddId}) => {
    const [sample, setSample] = useState(sampl)
    const [delBtnEnabled, setDelBtnEnabled] = useState(true);

    const revomeSampleInObj= async () => {
            setDelBtnEnabled(false);
            try {
                await removeFunction(objToAddId, sample.id);
                toast.success(<div>Химическое свойство: <i>{parse(sample.property.formula)}</i> успешно удалено</div>)
                setNeedReload(true)
            } catch {
                toast.error(<div>При удалении <i>{parse(sample.property.formula)}</i> произошла ошибка </div>)
            }
            
            setDelBtnEnabled(true);
    }

    return (
        <li key={sample.id} className='sampleItem'>
            <div className='sampleContainer'>
                <div className="sampleDataContainer">
                    <p>Тип значения: <i>{sampleDataType.find(dataType => dataType.value === sample.value.class).label}</i></p>
                    <p id="chemPropP">Химическое свойство: <i>{parse(sample.property.formula)}</i></p>
                </div>
                <div className="valueDataContainer">
                    {sample.value.class === sampleDataType[1].value ? 
                        <StringValueForm sample={sample} setSample={setSample}/>
                     : 
                        <RealNumberForm sample={sample} setSample={setSample}/>
                    }
                </div>
            </div>
            <img className="saveImg" src={saveItem}/>
            <img className="saveImg" src={deleteItem} isEnable={delBtnEnabled} onClick={revomeSampleInObj}/>
        </li>
    )
}