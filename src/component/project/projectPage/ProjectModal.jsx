import { useContext, useState } from 'react'
import './style.css'
import { Button } from '../../common/Botton/Button';
import { Modal } from '../../common/Modal/Modal';
import AsyncSelect from 'react-select/async'
import api from '../../../api';
import { AuthContext } from '../../../App';



const ProjectForm = ({project, setProject}) => {
    const {user} = useContext(AuthContext)


    async function loadUsers(inputValue, callback) {
        api.get("/users", {params: {login: inputValue}})
        .then(res => {
            console.log(user.login)
            console.log(res.data.filter(v => {return v.value !== user.login}))
            callback(res.data.filter(v => {return v.value !== user.login}).map(u => {return {value: u.login, label: u.login}}))
        })
        .catch(callback())
    }

    return (
        <form className='projectForm'>
                <input className='projectInput' placeholder='Название...' type="text" id="title" name="title" 
                    onInput={p => {let _project = {...project}; _project.title = p.target.value; setProject(_project)}}
                />
                <textarea className='projectInput' placeholder='Описание...' type="description" id="description" name="description"
                    onInput={p => {let _project = {...project}; _project.description = p.target.value; setProject(_project)}}
                />
                <AsyncSelect id="users" 
                    placeholder="Пользователи..."  
                    isMulti 
                    onChange={(p) =>{let _project = {...project}; _project.users = p ? p.map(u => {return u.value}) : []; setProject(_project)}}
                    loadOptions={loadUsers} 
                    defaultOptions={true}
                    />
        </form>
    )
}

export default function ProjectModal({isCreate, setModal, isModal}) {
    const [btnEnabled, setBtnEnabled] = useState(true);
    const [project, setProject] = useState({
        title: "",
        description: "",
        users: []
    })

    const saveProject = async() => {
        console.log(project)
        setBtnEnabled(false);
        await api.post("/projects", project)
            .then()
            .catch()
        setModal(false);
        setBtnEnabled(true);
    }

    return (
        <Modal
          isVisible={isModal}
          title={isCreate ? "Создать проект" : "Редактировать проект"}
          content={<ProjectForm project={project} setProject={setProject}/>}
          footer={<><Button onClick={() => {saveProject()}} disabled={!btnEnabled}>Создать</Button><Button onClick={() => setModal(false)}>Отмена</Button></>}
          onClose={() => setModal(false)}
        />
    )
}