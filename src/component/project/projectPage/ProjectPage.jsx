import { useContext, useEffect, useState } from 'react'
import './style.css'
import api from '../../../api';
import { AuthContext } from '../../../App';
import { NavBtn } from '../../common/Botton/NavBtn';
import { Button } from '../../common/Botton/Button';
import pencil from '../../../img/pencil.svg'
import { Modal } from '../../common/Modal/Modal';
import ProjectModal from './ProjectModal';
import { Navigate, Outlet } from 'react-router-dom';

export function Project({project}) { 

    return ( <div className='projectElem'>
            <NavBtn to={`/projects/${project.id}`} className='projectBtn'>{project.title}</NavBtn> <img src={pencil}/>
        </div>
        
    )

}

export default function ProjectsPage() {
    const [projects, setProjects] = useState([]);
    const [modal, setModal] = useState(false);
    const [isLoading, setLoading] = useState(true);

    const fetchData = async() => {
        const data = (await api.get("projects")).data
        console.log(data);
        setProjects(data.map(elem => {
            return ({
                id: elem.id,
                title: elem.title,
                description: elem.description,
                users: elem.users.map(user => {
                    return {
                        firstname: user.firstname,
                        lastname: user.lastname,
                        login: user.login
                    }
                })
            })
        }));
    }


    useEffect(() => {

        try {
            setLoading(true)
            fetchData()
            setLoading(false)
        } catch (ex) {
            console.log(ex)
        }  
    }, []);

    useEffect(() => {
        if(!modal)
          try {
            setLoading(true)
            fetchData()
            setLoading(false)
        } catch (ex) {
            console.log(ex)
        }  
    },[modal])

    return (
        <div className='projectPage'><div className='sideBarContainer'>
        <div className='projectsContainer'>{!isLoading ? <>{
            projects.length > 0 ?
            projects.map(elem => {
                return (
                    <Project key={elem.id} project={elem} />
                    )
                })
            : <div>Создай свой первый проект</div>}</>
            : <div>Loading...</div>
            }
        </div>
        <div className='addBtnContainer'>
                <Button className='addBtn' onClick={() => setModal(true)}><b>Добавить проект</b></Button>
                
        </div>
        <ProjectModal isModal={modal} setModal={setModal} isCreate={true}/>
        </div>
        <Outlet/>
        </div>

    )
}